# vim: fileencoding=utf-8:

import io
import json


class _config(object):
    pass


config = _config()
with io.open('config.json', 'r', encoding='utf-8') as f:
    for k, v in json.load(f).items():
        setattr(config, k, v)
