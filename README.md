# Danmaku

Client for Tsukkomi2.

## Notes

### PyGObject3 and cairo

This program uses PyGObject3, but official branch does not have support for Region in cairo foreign interface.
The needed cairo counterpart is in its repository, but pycairo haven't released new version for several years.

But you'll need `window.input_shape_combine_region(cairo_region)` to make the window click-through, so here's
some suggestions:

1. If you use recent Ubuntu, you have nothing to worry about. Ubuntu has this fix included in their
`python-gi-cairo` package.
2. On Arch (like me), you'll need to use `aur/python-cairo-git`, and rebuild `python-gobject` with the fix included:
    1. Get PKGBUILD for `python-gobject`: ```yaourt -G python-gobject```
    2. Download source and extract package, but don't build: ```makepkg -o```
    3. Get Ubuntu's fix [here](http://archive.ubuntu.com/ubuntu/pool/main/p/pygobject/pygobject_3.14.0-1.debian.tar.xz),
    extract `debian/patches/01_cairo_region.patch`
    4. Apply the patch onto previously extracted pygobject source.
    5. Build the package with `makepkg -o` and you are good to go.

If you are lazy, [here](https://gist.github.com/hexchain/e0847b4d8a41f47cc36a) is a modified PKGBUILD based on Arch official `pygobject-3.16.1` which automagically does the above
for you. Beware that this PKGBUILD is not updated, so please only use as a reference.