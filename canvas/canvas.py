# vim: fileencoding=utf-8:


from gi.repository import Gtk, Gdk, GLib
import cairo
import logging
import json

from .tsukkomi import Tsukkomi
from config import config

logger = logging.getLogger(__name__)


class Canvas:

    def __init__(self):

        def set_transparent(window):
            logger.debug("Setting transparent")

            # Transparent values are HERE
            transparent = Gdk.RGBA(1, 1, 1, 0)
            window.get_window().set_background_rgba(transparent)

        def set_clickthrough(window, cr):
            # No, this cannot be put together with set_transparent.
            # It will not function if not bound to `draw' event.
            # Not sure if this is another PyGObject bug, or I screwed up.
            logger.debug("Setting clickthrough")
            rect = cairo.RectangleInt(0, 0, 1, 1)
            region = cairo.Region(rect)
            if not region.is_empty():
                window.input_shape_combine_region(None)
                window.input_shape_combine_region(region)

            # And since this only need to be called once,
            # disable further invocation.
            window.disconnect_by_func(set_clickthrough)

        self._window = Gtk.Window(title="Danmaku canvas",
                                  type=Gtk.WindowType.TOPLEVEL)

        self._window.set_decorated(False)
        self._window.set_app_paintable(True)
        self._window.set_keep_above(True)
        self._window.set_skip_pager_hint(True)
        self._window.set_skip_taskbar_hint(True)
        self._window.set_accept_focus(False)
        self._window.set_focus_on_map(False)

        self._screen = self._window.get_screen()
        self._display = self._screen.get_display()

        # Search through all monitors and find the first suitable one
        # Currently does nothing
        monitors = self._screen.get_n_monitors()
        monitor = -1
        geometry = None
        for i in range(monitors):
            monitor_name = self._screen.get_monitor_plug_name(i)
            logger.info("Found monitor at %d, type %s", i, monitor_name)

            if 'LVDS' in monitor_name:
                logger.debug("Monitor %d looks like laptop panel", i)
                continue

            monitor = i
            break

        # Set window geometry based on selected monitor
        geometry = self._screen.get_monitor_geometry(monitor)
        if monitor == -1 or geometry is None:
            raise Exception("Valid monitor not found")

        self._geometry = geometry
        self._window.set_size_request(geometry.width, geometry.height)

        visual = self._screen.get_rgba_visual()
        if not visual:
            visual = self._screen.get_system_visual()
        self._window.set_visual(visual)

        # move to selected monitor
        self._window.move(self._geometry.x, self._geometry.y)

        GLib.timeout_add(1000 / config.fps, self.refresh, self._window)

        self._window.connect('draw', set_clickthrough)
        self._window.connect('draw', self.render)
        self._window.connect('realize', set_transparent)
        self._window.connect("delete-event", Gtk.main_quit)

        self._window.show_all()

        comment_height = config.font_size * 1.5 + 4 * config.outline
        self._num_slots = int(self._geometry.height / comment_height)
        self._tsukkomi = []
        self._slots = [None for _ in range(self._num_slots)]

    def refresh(self, widget):
        widget.queue_draw()
        return True

    def add_tsukkomi(self, data):
        if 'blacklisted' in data and data['blacklisted']:  # protect children
            return

        # text = data['text'].strip().splitlines()
        # for line in text:
        #     self._tsukkomi.append(Tsukkomi(data['text'], data['color']))

        self._tsukkomi.append(Tsukkomi(data['text'], data['color']))

    def render(self, widget, cr):
        def chase(c1, c2):
            if not c1:
                return -1
            t = (c1.x + c1.width) / c1.speed
            return (c1.x + c1.width -
                    c1.speed * t + c2.speed * t - self._geometry.width)

        comment_height = config.font_size + 4 * config.outline
        for comment in self._tsukkomi:
            if comment.x + comment.width < 0:  # finished
                if self._slots[comment.slot] is comment:  # the only one
                    self._slots[comment.slot] = None
                comment.slot = -1
                comment.onscreen = False
                comment.free()
                self._tsukkomi.remove(comment)
                continue

            elif comment.onscreen:  # on screen
                comment.x -= comment.speed

            else:
                y = 0
                mindistance = self._geometry.width
                minslot = -1
                for index in range(self._num_slots):
                    distance = chase(self._slots[index], comment)
                    if (not self._slots[index]) or (distance <= 0):
                        comment.onscreen = True
                        comment.x = self._geometry.width
                        comment.y = comment_height * index
                        self._slots[index] = comment
                        break
                    if distance < mindistance:
                        mindistance = distance
                        minslot = index
                    y += 1

                if not comment.onscreen:
                    comment.onscreen = True
                    comment.x = self._geometry.width
                    comment.y = comment_height * minslot
                    self._slots[minslot] = comment

                if not comment.drawn:
                    comment.draw()

            # logger.debug("Comment %s at %d, %d", comment.text, comment.x, comment.y)

            # logger.debug(repr(self._slots))
            cr.set_source_surface(comment.surface, comment.x, comment.y)
            cr.paint()

        # __import__('IPython').embed()
