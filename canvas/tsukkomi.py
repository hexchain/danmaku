# vim: fileencoding=utf-8:

import cairo
import logging
from math import ceil
from config import config

logger = logging.getLogger(__name__)


class Tsukkomi:
    class _extents(object):
        pass

    def __init__(self, text, color="ffffff", draw=True):
        def parse_color(strcolor):
            r, g, b = strcolor[:2], strcolor[2:4], strcolor[4:]
            r, g, b = [(int(n, 16)) / 255.0 for n in (r, g, b)]
            return (r, g, b)

        self.r, self.g, self.b = parse_color(color)
        self.text = text
        self.count = 0
        self.slot = -1
        self.extents = self._extents()
        self.drawn = False

        self.x, self.y = 0, 0
        self.slot = -1
        self.onscreen = False

        if draw:
            self.draw()

    def draw(self):
        def calculate_size(text):
            surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 0, 0)

            cr = cairo.Context(surface)
            cr.select_font_face(config.font_face)
            cr.set_font_size(config.font_size)
            result = zip(('xb', 'yb', 'width', 'height', 'xa', 'ya'),
                         cr.text_extents(text))
            surface.finish()
            for item in result:
                setattr(self.extents, item[0], int(ceil(item[1])))

        calculate_size(self.text)

        self.width = self.extents.width + 2 * config.outline * len(self.text)
        self.height = int(config.font_size * 1.5 + 4 * config.outline)

        self.surface = cairo.ImageSurface(cairo.FORMAT_ARGB32,
                                          self.width, self.height)

        cr = cairo.Context(self.surface)
        cr.select_font_face(config.font_face)
        cr.set_font_size(config.font_size)

        cr.move_to(0, config.font_size + 2 * config.outline)
        cr.set_source_rgba(self.r, self.g, self.b, 0.95)
        cr.text_path(self.text)
        cr.fill_preserve()
        cr.set_source_rgba(0, 0, 0, 0.95)
        cr.set_line_width(config.outline)
        cr.stroke()
        self.surface.flush()

        self.speed = (self.width + config.virtual_width) / \
            config.onscreen_time / (1000 / config.fps)
        self.drawn = True

    def free(self):
        self.extents = None
        self.surface.finish()
        self.surface = None
