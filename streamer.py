# vim: fileencoding=utf-8:

import websocket
import logging
import json
import time
import threading

from config import config

logger = logging.getLogger(__name__)


class Streamer(threading.Thread):
    def __init__(self, callback=None):
        super(Streamer, self).__init__()
        self.daemon = True
        self.name = 'Streamer'
        self._callback = callback
        self._alive = True

    def _on_message(self, ws, message):
        logger.debug("Got message: %s", message)
        try:
            data = json.loads(message)
        except ValueError:
            logger.info("Received invalid message, ignoring")
            return

        if 'type' in data and data['type'] == 'control':
            if data['reason'] == 0:
                setattr(config, 'code', data['code'])
            elif data['reason'] == 1:
                logging.error("Wrong API key supplied!")
                self._alive = False
            else:
                logger.info("Received invalid message, ignoring")
                self._alive = False
        elif 'type' in data and data['type'] == 'comment':
            self._callback(data)
        else:
            logger.info("Received invalid message, ignoring")

    def _on_open(self, ws):
        logger.debug("Opened connection")
        ws.send(json.dumps({'key': config.api_key}))

    def _on_error(self, ws, error):
        logger.error("Error: %s", error)

    def _on_close(self, ws):
        logger.debug("Closed connection")

    def run(self):
        logger.info("Starting %s...", self.name)

        while True:
            logger.info("Connecting")
            self._ws = websocket.WebSocketApp(
                    "wss://t.aoikaze.moe/api/stream",
                    on_message=self._on_message,
                    on_error=self._on_error,
                    on_close=self._on_close)
            self._ws.on_open = self._on_open
            try:
                if self._alive:
                    self._ws.run_forever()
                else:
                    return
            except KeyboardInterrupt:
                logging.info("INT received")
                break
            except websocket.WebSocketException as e:
                logging.error("Error: %s", repr(e))
                time.sleep(1)
