#!/usr/bin/env python3
# vim: fileencoding=utf-8:

from __future__ import (print_function, absolute_import)

from gi.repository import Gtk
import logging
import signal

from canvas import canvas
from streamer import Streamer

logging.basicConfig(level=logging.DEBUG)
logging.getLogger(__name__)


if __name__ == '__main__':
    window = canvas.Canvas()
    streamer = Streamer(window.add_tsukkomi)
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    streamer.start()
    Gtk.main()
